#!/usr/bin/env bash

exec gunicorn hero.wsgi:application \
  --config /var/gunicorn/gunicorn.py \
  "$@"
