"""
App specific configurations.
"""

SPECTACULAR_DEFAULTS = {
    'TITLE': 'Hero',
    'DESCRIPTION': 'Hero API',
    'SERVERS': [],
}
